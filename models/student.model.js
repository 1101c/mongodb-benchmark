const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const config = require('../config/database');

// Student Schema
const StudentSchema = mongoose.Schema({
    studentNumber: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    year: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    gpa: {
        type: String,
        required: true
    },
    program: {
        type: String,
        required: true
    },
});

const Student = module.exports = mongoose.model('Student', StudentSchema);

/**
 * Create a new Student
 * 
 * @param {*} newStudent New Student object
 * @param {*} _callback 
 */
module.exports.createStudent = function (_newStudent, _callback) {
    _newStudent.save(_callback);
};

/**
 * Get all Students
 * 
 * @param {*} _callback 
 */
module.exports.getStudents = function (_callback) {
    Student.find(_callback);
};

/**
 * Get Student by ID
 * 
 * @param {*} _id Student ID 
 * @param {*} _callback 
 */
module.exports.getStudentById = function (_id, _callback) {
    Student.findById(_id, _callback);
};

/**
 * Get Student by ID
 * 
 * @param {*} _studentNumber Student Slug 
 * @param {*} _callback 
 */
module.exports.getStudentByStudentNumber = function (_studentNumber, _callback) {
    Student.findOne({
        studentNumber: _studentNumber
    }, _callback);
};

/**
 * Delete Student by Student Number
 * 
 * @param {*} _studentNumber Student ID
 * @param {*} _callback 
 */
module.exports.deleteStudent = function (_studentNumber, _callback) {
    Student.deleteOne({
        studentNumber: _studentNumber
    }, _callback);
};