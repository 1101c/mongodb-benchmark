const express = require('express');
const router = express.Router();
const config = require('../config/database');
const Student = require('../models/student.model');
const Promise = require('bluebird');

/**
 * Create User
 */
router.post('/', (req, res) => {

    // Build User Object
    let newStudent = new Student({
        studentNumber: req.body.studentNumber,
        name: req.body.name,
        program: req.body.program,
        year: req.body.year,
        gpa: req.body.gpa,
        email: req.body.email,
    });

    Student.createStudent(newStudent, (err, student) => {
        if (err) {
            res.json({
                success: false,
                message: "Failed to create student",
                error: err
            });
        } else {
            res.json({
                success: true,
                message: "Student Created.",
                student: student
            });
        }
    });



});

/**
 * Get Student by Student Number
 */
router.get('/:studentNumber', (req, res) => {
    Student.getStudentByStudentNumber(req.params.studentNumber, (err, student) => {
        if (err) {
            res.json({
                success: false,
                message: "Failed to get student",
                error: err
            });
        } else {
            res.json({
                success: true,
                message: "Student Created.",
                student: student
            });
        }
    });
});

/**
 * Get All Student
 */
router.get('/', (req, res) => {
    Student.getStudents((err, students) => {
        if (err) {
            res.json({
                success: false,
                message: "Failed to get students",
                error: err
            });
        } else {
            res.json({
                success: true,
                students: students
            });
        }
    });
});

/**
 * Get Student by Student Number
 */
router.get('/delete/:studentNumber', (req, res) => {
    Student.deleteStudent(req.params.studentNumber, (err, student) => {
        if (err) {
            res.json({
                success: false,
                message: "Failed to delete student",
                error: err
            });
        } else {
            res.json({
                success: true,
                message: "Student Deleted.",
            });
        }
    });
});

module.exports = router;